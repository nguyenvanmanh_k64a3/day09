<?php  
   ob_start();
   session_start();
   if (isset($_GET['page'])) {
      $page = $_GET['page'];
   }else {
      $page = 'search';
   }
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="./src/style/style.css" type="text/css"> 
<script type="text/javascript" src='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js'></script>
<script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>

<title>Day 09</title>
<style>
@import url("https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400&display=swap");
</style>
</head>

<?php
$page = "multiplechoice";
 if (file_exists('src/views/'.$page.'.php')) {
  ?>
<body>
<?php 

            include_once 'src/controllers/controller.php';
            $controll = new Controller();
            $controll->Controllers();
            ?>
</body>
<?php
} else {
  echo "<h2 style='' class='err404'>Trang không tồn tại!</h2>";
}
?>
</html>
